const { request, response } = require("express");
const { compareSync } = require("bcryptjs");
const { sign } = require("jsonwebtoken");
const { Auth } = require("../config/config");
const UsuarioModel = require("../models/usuario");

async function login(req = request, res = response) {
  const { email, password } = req.body;

  try {
    const usuarioBD = await UsuarioModel.findOne({ email });
    if (usuarioBD) {
      if (compareSync(password, usuarioBD.password)) {
        sign(
          {
            id: usuarioBD.id,
            tipo: usuarioBD.rol,
            nombre: usuarioBD.nombre,
            apellidos: usuarioBD.apellido,
          },
          Auth.Token,
          { expiresIn: "5h" },
          (err, token) => {
            if (err){
              res.status(500).send({ mensaje: "hubo un error" });
            } 
            else res.send({ auth: true, token });
          }
        );
      } else {
        res.status(400).send({ mensaje: "Usuario o contraseña incorrecta" });
      }
    } else res.status(400).send({ mensaje: "no existe el usuario" });
  } catch (e) {
    res.status(500).send({ mensaje: "error interno del servidor", error: e });
  }
}

function validarJWT(req = request, res = response) {
  res.status(200).send({ auth: true });
}



module.exports = { login, validarJWT };
