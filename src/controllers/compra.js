const { request, response } = require("express");
const { verify } = require("jsonwebtoken");
const mongoose = require("mongoose");
const CompraModel = require("../models/compra");
const DetalleCompraModel = require("../models/detalle_compra");
const MetodoPagoModel = require("../models/metodo_pago");
const ProductoModel = require("../models/producto");
const UsuarioModel = require("../models/usuario");

async function generarCompra(req = request, res = response) {
  try {
    const { id } = req.params;

    if (!Number(id)) {
      return res.status(400).send({ mensaje: "Formato del ID Incorrecto" });
    }

    const datosCompra = req.body;

    if (
      !datosCompra.fecha ||
      !datosCompra.direccion ||
      !datosCompra.metodo_pago ||
      !datosCompra.detalle
    ) {
      return res.status(400).send({ mensaje: "Faltan datos obligatorios!" });
    }

    //Validando el metodo de pago
    const metodo = await MetodoPagoModel.findOne({
      codigo: datosCompra.metodo_pago,
    });

    if (!metodo) {
      return res.status(400).send({ mensaje: "El método de pago no existe" });
    }

    //Validando el usuario
    const usuario = await UsuarioModel.findOne({ id: id });

    if (!usuario) {
      return res.status(400).send({ mensaje: "El usuario no existe" });
    }

    if (datosCompra.detalle.length == 0) {
      return res
        .status(400)
        .send({ mensaje: "Faltan productos para la compra" });
    }
    const session = await mongoose.startSession();

    session.startTransaction();

    try {
      //Organizando Data De La Compra
      datosCompra.usuario = usuario;
      datosCompra.metodoPago = metodo;
      datosCompra.codigo_factura = generarCodigoFactura();

      datosCompra.valor_total = datosCompra.detalle.reduce(
        (a, b) => a + b.valor * b.cantidad,
        0
      );

      // Creando la compra
      const compraCreada = await CompraModel.create(datosCompra);
      const detallesCompraCreada = [];

      for (let i = 0; i < datosCompra.detalle.length; i++) {
        const detalle = datosCompra.detalle[i];

        const producto = await ProductoModel.findOne({
          codigo: detalle.producto,
        });

        if (!producto) {
          throw { error: "el producto no existe" };
        }

        const nuevoDetalle = {
          compra: compraCreada,
          producto: producto,
          cantidad: detalle.cantidad,
          valor: detalle.valor,
        };

        const detalleCreado = await DetalleCompraModel.create(nuevoDetalle);
        detallesCompraCreada.push(detalleCreado);
      }
      compraCreada.detalles = detallesCompraCreada;
      // Commit the changes
      await session.commitTransaction();
      return res.send({
        mensaje: "Compra realizada exitosamente",
        compra: compraCreada,
      });
    } catch (error) {
      // Rollback any changes made in the database
      await session.abortTransaction();
      throw error;
    } finally {
      session.endSession();
    }
  } catch (error) {
    return res.status(400).send({ mensaje: "Error interno", error });
  }

  function generarCodigoFactura() {
    const date = new Date();
    return (
      "" +
      (date.getMonth() + 1) +
      date.getDate() +
      date.getHours() +
      date.getMinutes() +
      date.getSeconds()
    );
  }
}

async function obtenerCompras(req = request, res = response) {
  const { id } = req.params;

  if (!Number(id)) {
    return res.status(400).send({ mensaje: "Formato del ID Incorrecto" });
  }

  //Validando el usuario
  const usuario = await UsuarioModel.findOne({ id: id });

  if (!usuario) {
    return res.status(400).send({ mensaje: "El usuario no existe" });
  }

  const lsCompras = await CompraModel.find({ usuario: usuario }).populate([
    "metodoPago",
  ]);

  return res.send(lsCompras);
}

async function obtenerDetallesCompra(req = request, res = response) {
  const { codigo } = req.params;

  const compra = await CompraModel.findOne({ codigo_factura: codigo });

  const lstDetalles = await DetalleCompraModel.find({ compra: compra }).populate("producto");

  return res.send(lstDetalles);
}

module.exports = { generarCompra, obtenerCompras,obtenerDetallesCompra };
