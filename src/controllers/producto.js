const { request, response } = require("express");
const { verify } = require("jsonwebtoken");
const ProductoModel = require("../models/producto");
const CategoriaModel = require("../models/categoria");
const path = require("path");

async function crearProducto(req = request, res = response) {
  const productoACrear = req.body;

  if (
    !productoACrear.codigo ||
    !productoACrear.nombre ||
    !productoACrear.categoria ||
    !productoACrear.marca ||
    !productoACrear.valor
  ) {
    res
      .status(400)
      .send({ mensaje: "Faltan datos obligatorios para crear el producto!" });
    return;
  }

  const producto = await ProductoModel.findOne({
    codigo: productoACrear.codigo,
  });

  if (producto) {
    res.status(400).send({
      mensaje: "Ya existe un producto con ese código: " + productoACrear.codigo,
    });
    return;
  }

  const categoria = await CategoriaModel.findOne({
    codigo: productoACrear.categoria,
  });

  if (!categoria) {
    res.status(400).send({ mensaje: "La categoria del producto no existe!" });
    return;
  }

  productoACrear.categoria = categoria;
  productoACrear.imagen = "";

  ProductoModel.create(productoACrear)
    .then((productoCreado) => {
      res.status(201).send({ mensaje: "Se creo el producto", productoCreado });
    })
    .catch(() => {
      res.status(500).send({ mensaje: "No se logro crear el producto" });
    });
}

async function guardarImagen(req = request, res = response) {
  try {
    const { codigoProducto } = req.query;

    //Validación de la imagen
    if (!req.files || Object.keys(req.files).length === 0) {
      return res.status(400).json({ mensaje: "No se encontro el archivo" });
    }

    // GET del producto
    const producto = await ProductoModel.findOne({ codigo: codigoProducto });

    if (!producto) {
      return res.status(400).json({
        mensaje: "No existe un producto con el código: " + codigoProducto,
      });
    }

    // Extrae el archivo segun el nombre (en este caso "archivo")
    const archivo = req.files.imagen;

    let nombreArchivo = codigoProducto;

    if (archivo.name.split(".").length > 0) {
      nombreArchivo += "." + archivo.name.split(".")[1];
    }

    const rutaDeCarga = path.join(__dirname, "../imagenes/", nombreArchivo);

    // Usa el metodo mv() para colocar el archivo en cualquier parte del backend
    archivo.mv(rutaDeCarga, (error) => {
      if (error) return res.status(500).send(error);

      // CARGA EL ARCHIVO
      producto.imagen = nombreArchivo;
      producto.save();

      const mensaje = {
        mensaje: "Archivo cargado corectamente",
      };
      res.send(mensaje);
    });
  } catch (error) {
    return res.status(400).send({ error: error });
  }
}

function obtenerImagen(req = request, res = response) {
  const { nombre } = req.query;
  const rutaDeLaImagen = path.join(__dirname, "../imagenes/", nombre);
  res.sendFile(rutaDeLaImagen);
}

async function obtenerProductos(req = request, res = response) {
  const lsProductos = await ProductoModel.find();

  res.send(lsProductos);
}

async function obtenerCategorias(req = request, res = response) {
  const lsCategorias = await CategoriaModel.find();

  res.send(lsCategorias);
}

module.exports = {
  obtenerCategorias,
  obtenerProductos,
  crearProducto,
  guardarImagen,
  obtenerImagen
};
