const { Router } = require("express")
const {crearUsuario, agregarDireccionEnvio,recuperarPassword} = require("../controllers/usuario")
const validarToken = require("../middlewares/auth")

const routerUsuario = Router()

// Crear/Registrar Usuario
routerUsuario.post("", crearUsuario)

//Agregar Dirección Al Usuario
routerUsuario.post("/direccion",[validarToken],agregarDireccionEnvio)

//Recuperar Contraseña
routerUsuario.post("/password/recovery",recuperarPassword)

module.exports = routerUsuario
