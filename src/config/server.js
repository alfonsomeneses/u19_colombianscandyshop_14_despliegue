const express = require("express");
const fileUpload = require("express-fileupload");
const cors = require("cors");
const routerUsuario = require("../routes/usuario");

const routerProducto = require("../routes/producto");
const conexionDB = require("./database");
const routerConfig = require("../routes/config");
const routerCompra = require("../routes/compra");
const dotenv = require("dotenv").config();
//const path = require("path")

class Server {
  constructor() {
    this.start();
    conexionDB();
    this.routes();
  }

  middlewares() {
    this.app.use(express.json());
    this.app.use(
      fileUpload({
        useTempFiles: true,
        tempFileDir: "/tmp/",
      })
    );
    this.app.use(cors());
    this.app.use(express.static("public"));
  }

  routes() {
    //Autenticación
    this.app.use("/api/auth", require("../routes/auth"));
    //Configuración BD
    this.app.use("/api/config", routerConfig);
    //Usuarios
    this.app.use("/api/usuario", routerUsuario);
    //Productos
    this.app.use("/api/producto", routerProducto);
    //Compras
    this.app.use("/api/compra", routerCompra);

     this.app.get("*", (req, res) => {
       res.sendFile(path.join(__dirname, "../../public/index.html"));
     });
  }

  start() {
    this.port = process.env.API_PORT;
    this.app = express();
    this.middlewares();
    this.app.listen(this.port, () => {
      console.log("Running API Rest , PORT:" + process.env.API_PORT);
    });
  }
}

module.exports = Server;
