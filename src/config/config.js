const Config = 
{
    DB:{
        Roles:[
            {codigo:"ADMIN",nombre:"Administrador"},
            {codigo:"USER",nombre:"Usuario"}
        ],
        Categorias:[
            {codigo:"CHOCO",nombre:"Chocolateria"},
            {codigo:"CONF",nombre:"Confitería"},
            {codigo:"REPO",nombre:"Repostería"},
            {codigo:"AREQ",nombre:"Arequipe"},
            {codigo:"TIPICO",nombre:"Dulces Típicos"}
        ],
        MetodosPagos:[
            {codigo:"EFECTIVO",nombre:"Efectivo"},
            {codigo:"DATAFONO",nombre:"Datáfono"}
        ]
    },
    Auth:{
        Token:"grupo14-colombians_candy_shop_2022"
    }
}

module.exports = Config;