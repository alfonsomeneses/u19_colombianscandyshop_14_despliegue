const { Schema, model } = require("mongoose")

const metodoPagoSchema = new Schema({
  codigo: String,
  nombre: String,
})

const MetodoPagoModel = model("metodo_pagos", metodoPagoSchema)

module.exports = MetodoPagoModel