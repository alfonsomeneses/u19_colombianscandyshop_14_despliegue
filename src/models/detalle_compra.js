const { Schema, model } = require("mongoose");

const detalleCompraSchema = new Schema({
  compra: {
    type: Schema.Types.ObjectId,
    ref: "compras",
  },
  producto: {
    type: Schema.Types.ObjectId,
    ref: "productos",
  },
  cantidad: Number,
  valor: Number
 
});

const DetalleCompraModel = model("detalle_compras", detalleCompraSchema);

module.exports = DetalleCompraModel;
