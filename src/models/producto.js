const { Schema, model } = require("mongoose")

const productoSchema = new Schema({
  codigo: String,
  nombre: String,
  descripcion: String,
  marca: String,
  valor: Number,
  imagen: String,
  categoria: {
    type: Schema.Types.ObjectId,
    ref: "categorias"
  }
})

const ProductoModel = model("productos", productoSchema)

module.exports = ProductoModel