import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import { BASE_URL } from "../config/constants";

export default function Login({ titulo, setIsAuth }) {
  const navigate = useNavigate();

  const [reload,setReload] = useState(false)

  const [errorMessage, setErrorMessage] = React.useState("");

  const handleClick = () => {
    setErrorMessage("Credenciales incorrectas")
  }

  useEffect(() => {
    
    const token =  localStorage.getItem("token");

    if (token) {
      navigate("/admin")
    }
    

    return () => {}
  }, [reload])

  async function formulario(event) {
    event.preventDefault(); // Evita que HTML reenvie una petición

    const { password, email } = event.target;

    try {
      const res = await axios.post(BASE_URL + "/auth/login", {
        email: email.value,
        password: password.value,
      });
      localStorage.setItem("token", res.data.token);
      
      if (setIsAuth){
        setIsAuth(true);
      }
      
      navigate("/admin");
    } catch (error) {
      console.log(error);
      alert("Credenciales incorrectas");
    }
  }

  return (
    <div className="login-page">
      <div className="login-box">
        <div className="login-logo">
          <a href="#">
            <b>{titulo}</b>
          </a>
        </div>
        <div className="card">
          <div className="card-body login-card-body">
            <p className="login-box-msg">Inicia sesión</p>
            <form onSubmit={formulario}>
              <div className="input-group mb-3">
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  placeholder="Correo electrónico"
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  placeholder="Contraseña"
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-7">
                  <div className="icheck-primary">
                    <input type="checkbox" id="remember" />
                    <label htmlFor="remember">Recuérdame</label>
                  </div>
                </div>
                <div className="col-5">
                  <button type="submit" className="btn btn-info btn-block">
                    Inicia sesión
                  </button>
                </div>
              </div>
            </form>

            {errorMessage && <div className="error"> {errorMessage} </div>}
            
            <p className="mb-1">
              <Link to="/forgot-password" className="text-center">
                Olvide mi contraseña
              </Link>
            </p>
            <p className="mb-0">
              <Link to="/register" className="text-center">
                ¿No tienes cuenta? Registrate
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
