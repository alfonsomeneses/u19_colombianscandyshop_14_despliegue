import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import { API_EndPoints } from "../../config/api._endpoints";
import { BASE_URL } from "../../config/constants";
import Header from "../../shared/header";

export default function ConsultaProductos() {
  const [productos, setProductos] = useState([]);
  const [reload, setReload] = useState(false);

  useEffect(() => {
    axios
      .get(BASE_URL + API_EndPoints.productos.consulta)
      .then((resp) => {
        setProductos(resp.data);
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {};
  }, [reload]);

  function cargarProducto(event) {

    const {id}= event.target
    
    const carrito = JSON.parse(localStorage.getItem("carrito_compra"))
    const productoSeleccionado = {
        producto:id,
        cantidad:1
    }
    
    const index = carrito.productos.findIndex((index)=>{
        return index.producto === productoSeleccionado.producto
    })

    let cantidad = 1;
    if (index === -1) {
        carrito.productos.push(productoSeleccionado);
    } else {
        carrito.productos[index].cantidad += 1;
        cantidad = carrito.productos[index].cantidad;
    }
    
  
    localStorage.setItem("carrito_compra", JSON.stringify(carrito));

    alert("Producto agregado al carrito. cantidad: "+cantidad)




  }
  return (
    <>
      <Header title="Lista de productos" path="producto" pathName="Productos" />
      <section className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <div className="card">
                <div className="card-header">Productos</div>
                <div className="card-body">
                  <table
                    id="table"
                    className="table table-bordered table-hover dataTable dtr-inline"
                  >
                    <tbody>
                      {productos.map((producto) => (
                        <tr key={producto._id}>
                          <td>
                            <div className="card">
                              <div className="card-header">
                                {producto.nombre } 
                              </div>
                              <div className="card-body">
                                <div className="row">
                                  <div className="col-8">
                                    <div className="card">
                                      <div className="card-body">
                                        <img
                                          src={
                                            BASE_URL +
                                            API_EndPoints.productos.verImagen +
                                            "nombre=" +
                                            producto.imagen
                                          }
                                          alt={producto.nombre}
                                          width="550"
                                          height="300"
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-4">
                                    <div className="card">
                                      <div className="card-body">
                                      <label>Ref: {producto.codigo}</label>
                                        <div>
                                        <label> {producto.marca}</label>
                                        </div>
                                        
                                        <div>
                                          <label> {producto.descripcion}</label>
                                        </div>
                                        <div>
                                          <label> ${producto.valor}</label>
                                        </div>
                                      </div>
                                      <div className="card-footer">
                                        <button
                                          id={producto._id}
                                          type="button"
                                          className="btn btn-info"
                                          onClick={cargarProducto}
                                        > 
                                          Cargar Al Carrito
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
