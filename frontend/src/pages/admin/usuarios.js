import axios from "axios"
import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { ALERT, BASE_URL } from "../../config/constants"
import Header from "../../shared/header"

export default function Usuarios() {
  const token = localStorage.getItem("token")
  const [usuarios, setUsuarios] = useState([])
  const [reload,setReload] = useState(false)

  useEffect(() => {
    axios
      .get(BASE_URL + "/cliente", { headers: { Authorization: token } })
      .then((res) => {
        setUsuarios(res.data)
      })
      .catch((err) => {
        alert("Hubo un error inesperado")
      })

    return () => {}
  }, [reload])

  function borrarUsuario(event) {
    const { id } = event.target

    // BORRAR EL USUARIO
    axios.delete(BASE_URL + "/cliente", {data: {id}, headers: { Authorization: token }})
      .then( (res)=>{
        setReload( !reload )
        ALERT.fire({icon: "warning", title: "Se borro el usuario"})
      } )
      .catch( (err)=>{
        ALERT.fire({icon: "error ", title: "Hubo un error al borrar el usuario"})
      } )

    
  }

  return (
    <>
      <Header title="Lista de usuarios" path="usuarios" pathName="Usuarios" />
      <section className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <div className="card">
                <div className="card-header">
                  Acá puedes ver o modificar usuarios
                </div>
                <div className="card-body">
                  <table
                    id="table"
                    className="table table-bordered table-hover dataTable dtr-inline">
                    <thead>
                      <tr>
                        <th
                          className="sorting sorting_asc"
                          tabIndex={0}
                          rowSpan={1}
                          colSpan={1}>
                          Tipo documento
                        </th>
                        <th
                          className="sorting"
                          tabIndex={0}
                          rowSpan={1}
                          colSpan={1}>
                          Numero Documento
                        </th>
                        <th
                          className="sorting"
                          tabIndex={0}
                          rowSpan={1}
                          colSpan={1}>
                          Nombres
                        </th>
                        <th
                          className="sorting"
                          tabIndex={0}
                          rowSpan={1}
                          colSpan={1}>
                          Apellidos
                        </th>
                        <th
                          className="sorting"
                          tabIndex={0}
                          rowSpan={1}
                          colSpan={1}>
                          Correo
                        </th>
                        <th
                          className="sorting"
                          tabIndex={0}
                          rowSpan={1}
                          colSpan={1}>
                          Opciones
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {usuarios.map((usuario) => (
                        <tr key={usuario._id} >
                          <td>{usuario.tipoDocumento}</td>
                          <td>{usuario.numDocumento}</td>
                          <td>{usuario.nombre}</td>
                          <td>{usuario.apellido}</td>
                          <td>{usuario.email}</td>
                          <td>
                            <div
                              className="btn-group"
                              role="group"
                              aria-label="Opciones">
                              <Link type="button" className="btn btn-info" to={"/admin/usuario/"+usuario._id}>
                                Modificar
                              </Link>
                              <button id={usuario._id} type="button" className="btn btn-danger" onClick={borrarUsuario}>
                                Borrar
                              </button>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
