import axios from "axios"
import React from "react"
import { useForm } from "react-hook-form"
import { Link, useNavigate } from "react-router-dom"
import { ALERT, BASE_URL } from "../config/constants"

export default function Register() {
  const navigate = useNavigate()

  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    setError,
    clearErrors,
  } = useForm()

  console.log(watch())

  function registrar(data) {
    // PETICION PARA CREAR USUARIO
    axios
      .post(BASE_URL + "/usuario", data)
      .then((res) => {
        ALERT.fire({
          icon: "success",
          title: "Se creo el usuario " + data.nombre,
        })
        navigate("/login")
      })
      .catch((err) => {
        if (err.response.data.mensaje) {
          ALERT.fire({
            icon: "error",
            title: err.response.data.mensaje,
          })
        }

        ALERT.fire({
          icon: "error",
          title: "Hubo un error al crear el usuario",
        })

        console.error(err)
      })
  }

  return (
    <div className="register-page">
      <div className="register-box">
        <div className="register-logo">
          <a href="../../index2.html">
            <b>Colombians Candy Shop</b>
          </a>
        </div>
        <div className="card">
          <div className="card-body register-card-body">
            <p className="login-box-msg">Registrate</p>
            <form onSubmit={handleSubmit(registrar)}>
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Nombres"
                  {...register("nombre")}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Apellidos"
                  {...register("apellido")}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Email"
                  {...register("email")}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="password"
                  className="form-control"
                  placeholder="Contraseña"
                  {...register("password")}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="password"
                  className="form-control"
                  placeholder="Verifica contraseña"
                  {...register("password2", {
                    required: true,
                    validate: (value) => {
                      if (value !== watch("password")) {
                        setError("passwordMatch")
                      } else {
                        clearErrors()
                      }
                    },
                  })}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              {errors.passwordMatch && (
                <span className="text-danger">
                  Las contraseñas no coinciden
                </span>
              )}
              <div className="row">
                <div className="col-8">
                  <div className="icheck-primary">
                    <input
                      type="checkbox"
                      id="agreeTerms"
                      name="terms"
                      defaultValue="agree"
                      {...register("terminos")}
                    />
                    <label htmlFor="agreeTerms">
                      Acepto los terminos de uso
                    </label>
                  </div>
                </div>
                <div className="col-4">
                  <button type="submit" className="btn btn-primary btn-block">
                    Registrar
                  </button>
                </div>
              </div>
            </form>
            <Link to="/login" className="text-center">
              Ya tengo una cuenta
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}
