import imagen from "../assets/logo192.png";

import "./sidebar.css";

import { FaUserFriends, FaUserPlus } from "react-icons/fa";

import React, { useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import axios from "axios";
import { payload, getPayload } from "../config/constants";
import { BASE_URL } from "../config/constants";

import { API_EndPoints } from "../config/api._endpoints";

export default function Sidebar() {
  const [itemUsuarioOpen, setItemUsuarioOpen] = useState(true);
  //let itemUsuarioOpen = true

  const [categorias, setCategorias] = useState([]);
  const [reload, setReload] = useState(false);

  useEffect(() => {
    axios
      .get(BASE_URL + API_EndPoints.categoias.consulta)
      .then((resp) => {
        setCategorias(resp.data);
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {};
  }, [reload]);

  return (
    <aside className="main-sidebar sidebar-dark-primary elevation-4 altura">
      <Link to="/" className="brand-link">
        <i className="fa-thin fa-candy"></i>
        <span className="brand-text font-weight-light">
          Colombians Candy Shop
        </span>
      </Link>
      <div>
        <div className="sidebar">
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img src={imagen} className="img-circle elevation-2" alt="User" />
            </div>
            <div className="info">
              <Link>
                {getPayload()?.nombre} {getPayload()?.apellidos}
              </Link>
            </div>
          </div>
        </div>
        <nav>
          <ul
            className="nav nav-pills nav-sidebar flex-column"
            data-widget="treeview"
            role="menu"
            data-accordion="false"
          >
            <li className="nav-item">
              <NavLink to="/producto" className="nav-link">
                <i className="fa fa-search nav-icon" />
                <p>Productos</p>
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/logout" className="nav-link" end>
               <i className="fa fa-power-off nav-icon" />
                <p>Cerrar Sesión</p>
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    </aside>
  );
}
