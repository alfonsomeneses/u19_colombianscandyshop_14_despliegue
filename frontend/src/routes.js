import React from "react";
import Home from "./pages/home";
import Login from "./pages/login";
import { Routes, Route } from "react-router-dom";
import PrivateRoute from "./config/auth";
import Admin from "./pages/admin";
import UpdateUsuario from "./pages/admin/update-usuario";
import Usuarios from "./pages/admin/usuarios";
import Register from "./pages/register";
import ForgotPassword from "./pages/forgot-password";
import UpdateProducto from "./pages/admin/update-producto";
import ConsultaProductos from "./pages/productos/consulta-productos";
import Logout from "./pages/logout";

export default function RoutesApp({ isAuth, setIsAuth }) {
  return (
    <Routes>
      <Route index element={<Home />} />
      
      <Route
        path="/"
        element={
          <PrivateRoute isAuth={isAuth}>
            <Admin />
          </PrivateRoute>
        }
      >
        <Route path="admin" /> 
        <Route path="admin/usuario/all" element={<Usuarios />} />
        <Route path="admin/usuario/:id" element={<UpdateUsuario />} />
        <Route path="admin/producto/:id" element={<UpdateProducto />} />
        <Route path="producto" element={<ConsultaProductos />} />
      </Route>
            
  

      <Route
        path="/login"
        element={
          <Login titulo={"Colombians Candy Shop"} setIsAuth={setIsAuth} />
        }
      />

      <Route path="/logout" element={<Logout />} />
      <Route path="/register" element={<Register />} />
      <Route path="/forgot-password" element={<ForgotPassword />} />
    </Routes>
  );
}
